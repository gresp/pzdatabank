#!/usr/bin/env python
import os
import json

for current_filename in os.listdir("."):
    print("Processing %s" % current_filename)
    f = open(current_filename)
    c = json.load(f)
    f.close()
    if "OutputSamplingInterval" not in c:
        print("Warning %s doas not contains OutputSamplingInterval")
        continue
    interval = c["OutputSamplingInterval"]
    del c["OutputSamplingInterval"]
    c["OutputSamplingRate"] = float("{:.4f}".format(1 / interval))
    f = open(current_filename, "w")
    json.dump(c, f, indent=2)
    f.close()


