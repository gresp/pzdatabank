#!/usr/bin/env python
import os
import json

for current_filename in os.listdir("."):
    if os.path.isdir(current_filename):
        continue
    print("Processing %s" % current_filename)
    f = open(current_filename)
    c = json.load(f)
    f.close()
    for stage in c:
        if "InputSamplingInterval" not in stage:
            print("Warning %s doas not contains InputSamplingInterval" % current_filename)
            continue
        interval = stage["InputSamplingInterval"]
        del stage["InputSamplingInterval"]
        stage["InputSamplingRate"] = float("{:.4f}".format(1 / interval))
    f = open(current_filename, "w")
    json.dump(c, f, indent=2)
    f.close()


